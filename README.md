# CODE TOOLS

Репозиторий с инструментами: статический анализ, линтер кода.

## Как подключить

изменить `composer.json`:
```json
"repositories": [
  {
    ...,
    {
      "type":"gitlab",
      "url":"https://gitlab.com/Dilun7495/php-code-tools.git"
    }
  }
],
"require-dev": {
...
"softphp/code-tools": "^1.0",
...
}
```

Дополнительно можете добавить в раздел скриптов команды для запуска функционала библиотеки:
```json
"scripts": {
  ...     
  "cs-fixer-show": "php ./vendor/bin/php-cs-fixer fix . --config=.php-cs-fixer.dist.php --diff --dry-run",
  "cs-fixer-fix": "php ./vendor/bin/php-cs-fixer fix . --config=.php-cs-fixer.dist.php --diff",
  "phpstan": "php ./vendor/bin/phpstan analyse -c phpstan.neon"
}
```
ВНИМАНИЕ:
- в `cs-fixer-show` и `cs-fixer-fix` указана проверка по всему проекту (`fix .`)
- Если у вас файл конфигурации другой, то добавьте `--config=you_config_path.php` вместо указанного

# [PHP CS FIXER](https://github.com/PHP-CS-Fixer/PHP-CS-Fixer/tree/master/doc/rules)
Создать в корне и добавить в гит файл `.php-cs-fixer.dist.php`:
```php
<?php

return include './vendor/softphp/code-tools/.php-cs-fixer.dist.php';
```


# [PHPSTAN](https://phpstan.org/config-reference)

Создать файл в корневой директории `phpstan.neon`:
```yaml
includes:
	- vendor/softphp/code-tools/phpstan.neon.dist
parameters:
    paths:
        - src
        - tests
```
ВНИМАНИЕ: `paths` - это проверяемые директории в проекте.

Посмотреть количество ошибок локально

```shell
php ./vendor/bin/phpstan analyse -c phpstan.neon
```
