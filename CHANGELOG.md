# Changelog

The format is based on [Keep a Changelog v1.1](https://keepachangelog.com/en/1.1.0/)

## 1.0.0
### Added
- добавлен php-cs-fixer
- добавлен phpstan