<?php

declare(strict_types = 1);

return (new PhpCsFixer\Config())
    ->setRiskyAllowed(true)
    ->setRules([
        '@PSR12'                                 => true,
        '@PSR12:risky'                           => true,
        '@PHP84Migration'                        => true,
        '@PHPUnit84Migration:risky'              => true,
        '@DoctrineAnnotation'                    => true,

        // Declare
        'blank_line_after_opening_tag'           => true,
        'fully_qualified_strict_types'           => true,
        'declare_equal_normalize'                => ['space' => 'single'],
        'declare_strict_types'                   => true,
        'no_unused_imports'                      => true,
        'global_namespace_import'                => [
            'import_classes'   => true,
            'import_functions' => true,
            'import_constants' => true,
        ],
        'ordered_imports'                        => [
            'sort_algorithm' => 'alpha',
            'imports_order'  => ['class', 'const', 'function'],
        ],
        'void_return'                            => true,
        'single_class_element_per_statement'     => true,

        // PhpDoc & comment
        'phpdoc_add_missing_param_annotation'    => ['only_untyped' => true],
        'phpdoc_order'                           => ['order' => ['param', 'return', 'throws']],
        'phpdoc_align'                           => true,
        'phpdoc_no_useless_inheritdoc'           => true,
        'no_superfluous_phpdoc_tags'             => true,
        'no_empty_phpdoc'                        => true,
        'multiline_comment_opening_closing'      => true,

        // PhpUnit
        'php_unit_method_casing'                 => ['case' => 'camel_case'],

        // Other
        'array_indentation'                      => true,
        'binary_operator_spaces'                 => [
            'operators' => [
                '='  => 'align_single_space_minimal',
                '=>' => 'align_single_space_minimal',
                '|'  => 'no_space',
            ],
        ],
        'compact_nullable_typehint'              => true,
        'concat_space'                           => ['spacing' => 'one'],
        'explicit_string_variable'               => true,
        'increment_style'                        => ['style' => 'pre'],
        'method_chaining_indentation'            => true,
        'multiline_whitespace_before_semicolons' => true,
        'no_alternative_syntax'                  => true,
        'no_useless_else'                        => true,
        'no_useless_return'                      => true,
        'ordered_class_elements'                 => ['sort_algorithm' => 'none'],
        'ternary_to_null_coalescing'             => true,
        'yoda_style'                             => ['equal' => true, 'identical' => true, 'less_and_greater' => null],
        'strict_comparison'                      => true,
        'strict_param'                           => true,
        'blank_line_before_statement'            => [
            'statements' => [
                'break',
                'return',
                'continue',
                'try',
                'do',
                'exit',
                'goto',
                'if',
                'switch',
                'yield',
            ],
        ],
    ])
    ->setFinder(
        PhpCsFixer\Finder::create()
            ->exclude('vendor')
            ->in(__DIR__)
    );
